public class ContactService implements IContactService{
    private IContactDao contactDao;

    @Override
    public Contact creerContact(String nom) throws ContactException {
        Contact newContact = null;
        if(nom == null || nom.trim().length() < 3 || nom.trim().length() > 10) {
            throw new ContactException("Nom incorrect");
        } else if(contactDao.isContactExist(nom)) {
            throw new ContactException("Nom déjà existant");
        } else {
            newContact = new Contact();
            newContact.setNom(nom);
            contactDao.add(newContact);
        }

        return newContact;
    }

    @Override
    public boolean supprimerContact(String nom) throws ContactException {
        boolean exist = this.contactDao.isContactExist(nom);
        if(!exist) {
            throw new ContactException("Contact non existant");
        } else {
            boolean supprimer = this.contactDao.remove(nom);
            if(supprimer) {
                return true;
            } else {
                return false;
            }
        }
    }
}
