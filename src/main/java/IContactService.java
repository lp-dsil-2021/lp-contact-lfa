public interface IContactService {
    Contact creerContact(String nom) throws ContactException;
    boolean supprimerContact(String nom) throws ContactException;
}