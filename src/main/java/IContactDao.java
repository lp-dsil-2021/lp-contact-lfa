public interface IContactDao {
    boolean isContactExist(String name);
    void add(Contact contact);
    boolean remove(String nom);
}
