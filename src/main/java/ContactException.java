public class ContactException extends Exception{
    public ContactException(String message) {
        super(message);
        System.out.println(message);
    }
}
