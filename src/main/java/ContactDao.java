import java.util.List;

public class ContactDao implements IContactDao{
    private List<Contact> contacts;


    public ContactDao() {

    }

    @Override
    public boolean isContactExist(String name) {
        boolean ret = false;
        for(int i = 0; i < contacts.size(); i++) {
            if(name.equalsIgnoreCase(contacts.get(i).getNom())) {
                ret = true;
            }
        }

        return ret;
    }

    @Override
    public void add(Contact contact) {
        this.contacts.add(contact);
    }

    @Override
    public boolean remove(String nom) {
        boolean ret = false;
        for(int i = 0; i < this.contacts.size(); i++) {
            if(nom.equalsIgnoreCase(this.contacts.get(i).getNom())) {
                this.contacts.remove(i);
                ret = true;
            }
        }

        return ret;
    }
}
