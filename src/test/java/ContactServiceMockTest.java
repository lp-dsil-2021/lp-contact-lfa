import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceMockTest {

    @Mock
    private IContactDao contactDao;

    @InjectMocks
    private ContactService contactService = new ContactService();

    @Test(expected = ContactException.class)
    public void shouldFailOnDuplicateEntry() throws ContactException {
        //Définition du Mock
        Mockito.when(contactDao.isContactExist("Thierry")).thenReturn(true);

        contactService.creerContact("Thierry");

    }

    @Test
    public void shouldPass() throws ContactException {
        //Définition du Mock
       // Mockito.when(contactDao.isContactExist("Thierry")).thenReturn(false);

        //Test
        //Assertions.assertEquals("Thierry", contactService.creerContact("Thierry"));
    }
}