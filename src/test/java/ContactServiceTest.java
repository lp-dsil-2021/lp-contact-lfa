import org.junit.jupiter.api.Assertions;
import org.junit.Test;

public class ContactServiceTest {
    IContactService service = new ContactService();

    @Test(expected = ContactException.class)
    public void shouldFailIfNull() throws ContactException {
        service.creerContact(null);
    }

    @Test(expected = ContactException.class)
    public void shouldFailIfEmpty() throws ContactException {
        service.creerContact("");
    }


}
